﻿using App1.Helpers;
using Syncfusion.SfCalendar.XForms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            LoadIni();
        }

        private void LoadIni()
        {
            LoadApi();
        }

        private async void LoadApi()
        {
            try
            {
                var api = new Api();
                var result = await api.GetList<RootObject>();
                if (result.IsSuccess)
                {

                    var dates = (List<RootObject>)result.Result;
                    SfCalendar cal = new SfCalendar();
                    List<DateTime> black_dates = new List<DateTime>();
                    CalendarEventCollection collection = new CalendarEventCollection();


                    //ScheduleAppointmentCollection scheduleCollection = new ScheduleAppointmentCollection();

                    foreach (var item in dates)
                    {
                        DateTime dat = DateTime.Parse(item.date);
                        CalendarInlineEvent event1 = new CalendarInlineEvent();
                        event1.StartTime = new DateTime(dat.Year, dat.Month, dat.Day, 0, 0, 0);
                        event1.EndTime = new DateTime(dat.Year, dat.Month, dat.Day, 23, 59, 59);
                        event1.Subject = item.localName;
                        event1.Color = Color.Green;
                        collection.Add(event1);

                        
                        black_dates.Add(DateTime.Parse(item.date));
                    }
                    calendar.ShowInlineEvents = true;
                    calendar.DataSource = collection;
                    //    scheduleCollection.Add(new ScheduleAppointment {
                    //         StartTime= new DateTime(dat.Year, dat.Month, dat.Day, 0, 0, 0),
                    //         EndTime= new DateTime(dat.Year, dat.Month, dat.Day, 23, 59, 50),
                    //         Color=Color.Green,
                    //         IsAllDay=true,
                    //         Subject=item.localName,

                    //});
                    //schedule1.DataSource = scheduleCollection;
                    //calendar.BlackoutDates = black_dates;
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.ToString() + ex.InnerException, "Accept");
            }
        }
    }
}
