﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1.Helpers
{
    public class RootObject
    {
        public string date { get; set; }
        public string localName { get; set; }
        public string name { get; set; }
        public string countryCode { get; set; }
        public bool @fixed { get; set; }
        public bool global { get; set; }
        public string counties { get; set; }
        public string launchYear { get; set; }
        public string type { get; set; }
    }
}
