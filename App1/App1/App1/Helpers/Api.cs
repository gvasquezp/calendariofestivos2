﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App1.Helpers
{
    public class Api
    {
        public async Task<Response> GetList<T>()
        {
            //var client = new RestClient("https://public-holiday.p.rapidapi.com/2019/US");
            //var request = new RestRequest(Method.GET);
            //request.AddHeader("x-rapidapi-host", "public-holiday.p.rapidapi.com");
            //request.AddHeader("x-rapidapi-key", "3c628866cbmshd371a590e391486p19280cjsnb3a9de0e387a");
            //IRestResponse response = client.Execute(request);

            try
            {
                // Url which is used for making the get request
                string url = "https://public-holiday.p.rapidapi.com/2019/CO";
                //Creating a Http client
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("x-rapidapi-host", "public-holiday.p.rapidapi.com");
                client.DefaultRequestHeaders.Add("x-rapidapi-key", "3c628866cbmshd371a590e391486p19280cjsnb3a9de0e387a");

                // Making the request and putting the response into responce
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();
                // Parsing the data using NewtonJson package
                var list = JsonConvert.DeserializeObject<List<T>>(result);

                if (!response.IsSuccessStatusCode)
                {
                    return new Response {  IsSuccess=false};
                }

               
                return new Response { IsSuccess = true, Result = list };
                




            }
            catch (Exception)
            {
                

                    return new Response { IsSuccess = false };
                
            }


        }




    }

    
}
